# Zadanie
Stworzyć aplikację zawierającą jeden ekran, z Tabelką wyświetlającą treść recenzji produktu o id p1144563101 (Fifa 18) z Empiku.
Recenzje mają być paginowane, to znaczy na starcie aplikacji i przy każdym scrollowaniu na koniec tabelki ma zostać dociągnięta, a następnie wyświetlona kolejna strona z modelami recenzji. Wyjątkiem od tej sytuacji jest oczywiście sytuacja w której zaciągneliśmy już wszystkie strony.
### API
Endpoint pod który należy wysłać requesty: https://meaqa.mocklab.io/product/p1144563101/reviews, jedynym jego parametrem przekazywanym w query jest "offset", będący liczbą całkowitą, wskazujący na pierwszy indeks recenzji znajdujący się na stronie która mamy zamiar pobrać.
Ilością modeli na stronie zarządza backend.
W odpowiedzi z endpointu przychodzi poniższa struktura:

![Alt text](reviewAPI.png?raw=true "Empik Review Api")

Na powyższym obrazku parametry ozaczone gwiazdką są wymagane, to znaczy na pewno je dostaniemy i będą określone.
Struktura pagingInfo daje nam informacje o aktualnym stanie stronicowania. NextOffset = nil rozumiemy jako brak kolejnych stron, w przeciwnym razie, jest to indeks pierwszej recenzji, której jeszcze nie pobraliśmy.

### UI
W komórce tabeli należy wyświetlić tylko tekst recenzji, ważne jest żeby komórka tabeli miała odpowiednią wysokkość pozwalającą na zmieszczenie całego tekstu. UI może być bardzo prosty, ale warto zadbać o to, żeby był intuicyjny dla użytkownika.

### Pozostałe wymagania
Bardzo zależy nam, na takim przemyśleniu i implementacji rozwiązania, żeby sam mechanizm paginacji był możliwie reużywalny, to znaczy, żebyśmy stosunkowo łatwo mogli stworzyć inny widok z tabelką, która tym razem pobierze stronę obrazków, produktów, wyników wyszukiwania.

Aplikacja ma być napisana w Swift, i wspierać iOS 12. Rozwiązanie należy umieścic w dowolnym repozytorium i wysłać do niego linka mailem po ukończeniu zadania. Jeśli zajdzie taka potrzeba można korzystać z dowolnych "dependecy managerów", i bibliotek (z zachowaniem zdrowego rozsądku, to znaczy nie chodzi nam o znalezienie bilioteki rozwiązującej problem paginacji).
